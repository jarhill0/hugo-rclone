FROM ubuntu:latest

ARG hugo_version=0.72.0

RUN apt-get update \
    && apt-get install -y rclone wget \
    && wget https://github.com/gohugoio/hugo/releases/download/v${hugo_version}/hugo_${hugo_version}_Linux-64bit.deb \
    && dpkg -i hugo_${hugo_version}_Linux-64bit.deb \
    && rm -rf /var/lib/apt/lists/*

# https://gist.github.com/alkrauss48/2dd9f9d84ed6ebff9240ccfa49a80662
RUN mkdir -p /home/build

RUN groupadd -r build \
    && useradd -r -g build -d /home/build -s /sbin/nologin -c "main user" build

ENV HOME=/home/build
RUN chown -R build:build $HOME

USER build
